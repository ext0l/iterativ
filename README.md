# iterativ

Someday, this will be a roguelike in the vein of Dungeon Crawl Stone Soup.

This wouldn't exist at all without Herbert Wolverson's amazing [Rust roguelike
tutorial](https://bfnightly.bracketproductions.com/).
