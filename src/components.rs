use crate::geometry::*;
use crate::tiles::TileId;
use hecs::{Entity, World};
use sdl2::pixels::Color;

/// Models turn order. Entities start out with a given initiative, which ticks down by 1 every
/// turn. When it reaches 0, that entity is ready to move.
#[derive(Debug, Copy, Clone)]
pub struct Initiative {
    pub current: i32,
    pub initial: i32,
}

impl Initiative {
    pub fn new(initial: i32) -> Self {
        Initiative {
            current: initial,
            initial,
        }
    }

    /// Ticks down the initiative count. Returns true if the entity is ready (i.e., if its
    /// initiative reached 0). Note that this will also reset the initiative to its initial value.
    pub fn tick(&mut self) -> bool {
        self.current -= 1;
        if self.current <= 0 {
            self.current = self.initial;
            true
        } else {
            false
        }
    }
}

/// A marker component for entities that are ready. The
#[derive(Default, Debug, Copy, Clone)]
pub struct Ready;

/// A marker component for the player entity. There is at most one entity with this ID, and its ID
/// is also stored in the PlayerId resource.
#[derive(Default, Debug, Copy, Clone)]
pub struct IsPlayer;

/// Represents the human-readable name of something. Should be all-lowercase (except for proper
/// nouns, of course).
#[derive(Debug, Clone)]
pub struct Name {
    pub name: String,
}

/// The position of a given entity inside the world.
#[derive(Debug, Copy, Clone)]
pub struct Position(pub WorldPoint);

/// A tag to indicate that an entity cannot be moved through.
#[derive(Default, Debug, Copy, Clone)]
pub struct BlocksMovement;

/// This entity has some kind of visual representation.
#[derive(Debug, Clone)]
pub struct Visible {
    pub tile_id: TileId,
    pub color: Color,
    pub alpha: f32,
}

// All AttackIntent components indicate that the entity is going to attack/etc this turn. These will
// later be resolved by systems.

/// Indicates that this entity wants to attack the target. Resolved by AttackSystem.
#[derive(Copy, Clone, Debug)]
pub struct AttackIntent {
    pub target: Entity,
}

/// Indicates that this entity wants to move. Resolved by MovementSystem. Only makes sense on entities
/// that have a Position.
#[derive(Copy, Clone, Debug)]
pub struct MoveIntent(pub WorldVector);

/// Stats that are relevant for combat.
#[derive(Copy, Clone, Debug)]
pub struct CombatStats {
    pub max_hp: i32,
    pub hp: i32,
    pub attack: i32,
}

/// Indicates that damage is going to be applied to the given entity this tick.
#[derive(Clone, Debug)]
pub struct QueuedDamage(pub Vec<i32>);

impl QueuedDamage {
    pub fn add(world: &mut World, who: Entity, amount: i32) {
        if world.get_mut::<Self>(who).is_err() {
            world.insert_one(who, QueuedDamage(vec![amount])).unwrap();
        } else {
            world.get_mut::<Self>(who).unwrap().0.push(amount);
        }
    }
}

pub use crate::viewshed::Viewshed;
