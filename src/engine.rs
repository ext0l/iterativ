use anyhow::{Context, Result};
use hecs::{Entity, World};
use itertools::Itertools;
use log::{info, warn};
use resources::{Ref, Resources};
use sdl2::pixels::Color;
use std::time::Duration;

use crate::geometry::*;
use crate::map::Map;
use crate::systems::*;
use crate::{ability::Ability, ai};
use crate::{components::*, tiles::TileId};

#[derive(Debug, Copy, Clone)]
pub struct PlayerId(pub Entity);

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Action {
    Move { motion: WorldVector },
    Attack { target: Entity },
    Ability { ability: Ability, target: Entity },
}

pub struct Engine {
    pub world: World,
    pub resources: Resources,
    /// A dispatcher for systems that should be run each time we go
    /// through the inner tick loop.
    tick_loop: Dispatcher,
    /// A dispatcher for systems that should be run once for each tick.
    once_per_tick: Dispatcher,
}

/// What sort of things are valid targets?
#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub enum TargetingMode {
    /// The target has to be an enemy.
    Enemy,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Targeting {
    pub ability: Ability,
    pub range: i32,
    pub mode: TargetingMode,
    pub targets: Vec<Entity>,
    pub target_idx: Option<usize>,
}

impl Targeting {
    pub fn new(ability: Ability, range: i32, mode: TargetingMode, targets: Vec<Entity>) -> Self {
        assert!(
            !targets.is_empty(),
            "tried to make empty target list for {:?}",
            ability
        );
        Self {
            ability,
            range,
            mode,
            targets,
            target_idx: None,
        }
    }

    pub fn target(&self) -> Option<Entity> {
        self.target_idx.map(|idx| self.targets[idx])
    }

    pub fn next_target(&mut self) {
        self.target_idx = Some(match self.target_idx {
            None => 0,
            Some(idx) => (idx + 1) % self.targets.len(),
        });
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum LoopState {
    Looping,
    WaitingForPlayer,
    GameOver,
    Targeting(Targeting),
}

#[derive(Debug, Copy, Clone)]
pub struct FrameDuration(pub Duration);

/// Indicates which layers of the graphics are dirty and need rerendering.
#[derive(Debug, Copy, Clone)]
pub struct DirtyLayers {
    pub map: bool,
}

impl DirtyLayers {
    fn new() -> Self {
        Self { map: true }
    }

    pub fn mark_clean(&mut self) {
        self.map = false;
    }
}

impl Engine {
    pub fn new() -> Result<Self> {
        let mut world = World::new();
        let mut resources = Resources::new();
        let mut tick_loop = Dispatcher::new();
        tick_loop.push(InitiativeSystem);
        tick_loop.push(MovementSystem);
        tick_loop.push(MapUpdateSystem);
        tick_loop.push(AttackSystem);
        tick_loop.push(DamageSystem);
        tick_loop.push(DeathSystem);
        tick_loop.push(ViewshedSystem);
        tick_loop.setup(&mut world, &mut resources)?;
        let mut once_per_tick = Dispatcher::new();
        once_per_tick.push(ParticleSystem);
        once_per_tick.setup(&mut world, &mut resources)?;
        resources.insert(LoopState::Looping);
        resources.insert(DirtyLayers::new());
        resources.insert(FrameDuration(Duration::from_millis(16)));
        Ok(Engine {
            world,
            resources,
            tick_loop,
            once_per_tick,
        })
    }

    pub fn set_player_action(&mut self, action: Action) {
        if let Some(action) = self.normalize_action(action) {
            self.resources.insert(ai::PlayerAction(action));
        }
    }

    /// Translates a 'high-level' action into a low-level one. This implements things like moving
    /// into an entity to attack it.
    ///
    /// If this returns `None`, then the action requested is impossible (e.g., moving into a wall).
    fn normalize_action(&mut self, action: Action) -> Option<Action> {
        match action {
            Action::Move { motion } => {
                let map = self.resources.get::<Map>().unwrap();
                let player_id = *self.resources.get::<PlayerId>().unwrap();
                let player_pos = self.world.get::<Position>(player_id.0).unwrap().0;
                let target = player_pos + motion;
                if map.is_blocked(target) {
                    if let Some(blocker) = map.blockers(target) {
                        Some(Action::Attack { target: blocker })
                    } else {
                        None
                    }
                } else {
                    Some(action)
                }
            }
            _ => Some(action),
        }
    }

    pub fn perform(&mut self, entity: Entity, action: Action) {
        info!("{:?} performing {:?}", entity, action);
        if entity == self.player() {
            // Clear out the player's action, since we're about to execute it.
            self.resources.remove::<ai::PlayerAction>();
            // We're no longer waiting for the player.
            self.resources.insert(LoopState::Looping);
        }

        match action {
            Action::Move { motion } => {
                self.world
                    .insert_one(entity, MoveIntent(motion))
                    .expect("failed to insert move intent");
            }
            Action::Attack { target } => {
                self.world
                    .insert_one(entity, AttackIntent { target })
                    .expect("failed to insert attack intent");
            }
            Action::Ability { ability, target } => {
                self.execute_ability(entity, ability, target);
            }
        }
    }

    fn find_actor(&self) -> Option<(Entity, Action)> {
        let player = self.player();
        for (entity, ai) in self
            .world
            .query::<&mut ai::AIComponent>()
            .with::<Ready>()
            .into_iter()
        {
            if let Some(action) = ai.0.decide(&self.world, &self.resources, entity) {
                return Some((entity, action));
            } else if entity == player {
                *self.resources.get_mut().unwrap() = LoopState::WaitingForPlayer;
                return None;
            }
        }
        None
    }

    // The game loop works like this: we find an entity that has something that it can do. If at
    // any point the player is ready but doesn't have anything it can do, we enter the 'waiting for
    // player' state. In this state, the initiative system does nothing. And even if there are mobs
    // that are also ready, we'll eventually execute their actions given enough runs through
    // tick(), meaning that we'll get into a situation where the player is the only mob that's
    // ready, but it's not doing anything; i.e., we're waiting for playre input.
    //
    // Then, when the player inputs something, the next tick through will finally have the player's
    // "AI" return an action, causing us to leave `WaitingForPlayer` and resume normal engine
    // execution.

    pub fn tick(&mut self) -> Result<()> {
        self.once_per_tick
            .run(&mut self.world, &mut self.resources)?;
        if matches!(*self.loop_state(), LoopState::Targeting { .. }) {
            return Ok(());
        }
        // We run this in a loop so that we're not stuck ticking once per frame. TODO: Move the
        // tick-until-waiting into a separate function or something.
        loop {
            if let Some((entity, action)) = self.find_actor() {
                self.world.remove_one::<Ready>(entity)?;
                self.perform(entity, action);
            }
            self.tick_loop.run(&mut self.world, &mut self.resources)?;
            self.world.flush();
            if *self.loop_state() != LoopState::Looping {
                return Ok(());
            }
        }
    }

    pub fn loop_state(&self) -> Ref<'_, LoopState> {
        self.resources.get::<LoopState>().unwrap()
    }

    pub fn dirty_layers(&self) -> DirtyLayers {
        *self.resources.get::<DirtyLayers>().unwrap()
    }

    pub fn mark_layers_clean(&self) {
        self.resources
            .get_mut::<DirtyLayers>()
            .unwrap()
            .mark_clean()
    }

    pub fn player(&self) -> Entity {
        let player_id = *self.resources.get::<PlayerId>().unwrap();
        player_id.0
    }

    pub fn start_targeting(&mut self, ability: Ability) {
        *self.resources.get_mut().unwrap() = LoopState::Targeting(Targeting::new(
            ability,
            ability.range(),
            ability.targeting_mode(),
            self.find_targets(ability),
        ))
    }

    pub fn cancel_targeting(&mut self) {
        *self.resources.get_mut().unwrap() = LoopState::WaitingForPlayer;
    }

    pub fn next_target(&mut self) {
        if let LoopState::Targeting(ref mut tgt) = *self.resources.get_mut().unwrap() {
            tgt.next_target();
        } else {
            panic!("tried to get next target while not targeting");
        }
    }

    fn find_targets(&mut self, ability: Ability) -> Vec<Entity> {
        match ability.targeting_mode() {
            TargetingMode::Enemy => {
                let range = ability.range();
                let player = self.player();
                let player_position = self.world.get::<Position>(player).unwrap().0;
                let map = self.resources.get::<Map>().unwrap();
                (-range..=range)
                    .cartesian_product(-range..=range)
                    .map(|(dx, dy)| player_position + WorldVector::new(dx, dy))
                    .filter(|pos| map.is_valid(*pos))
                    .flat_map(|pos| map.entities[map.idx(pos)].iter().cloned())
                    // XXX: need to check if this is actually an
                    // enemy; add an Enemy tag component or
                    // something
                    .filter(|entity| entity != &player)
                    .filter(|entity| self.world.get::<CombatStats>(*entity).is_ok())
                    .collect()
            }
        }
    }

    pub fn select_ability_target(&mut self) {
        let state = self
            .resources
            .get_mut::<LoopState>()
            .expect("couldn't get state")
            .clone();
        if let LoopState::Targeting(targeting) = state {
            if let Some(target) = targeting.target() {
                self.set_player_action(Action::Ability {
                    ability: targeting.ability,
                    target,
                });
            }
        } else {
            panic!(
                "tried to execute ability while not targeting; was in state {:?}",
                state
            );
        }
        *self.resources.get_mut().unwrap() = LoopState::Looping;
    }

    fn execute_ability(&mut self, who: Entity, ability: Ability, target: Entity) {
        match ability {
            Ability::GapCloser => {
                let map = self.resources.get::<Map>().unwrap();
                let source_pos = self.world.get::<Position>(who).unwrap().0;
                let target_pos = self.world.get::<Position>(target).unwrap().0;
                let dest = map
                    .adjacent(target_pos)
                    .filter(|dest| *dest == source_pos || !map.is_blocked(*dest))
                    // We sort by square length here because it's
                    // more 'intuitive'; filtering by Linf would mean
                    // that if you were a knights-move away, you could
                    // teleport either diagonally adjacent or
                    // orthogonally adjacent, which is weird.
                    .min_by_key(|dest| (*dest - source_pos).square_length());
                if let Some(dest) = dest {
                    if let Ok(visible) = self.world.get::<Visible>(who) {
                        let (r, g, b) = visible.color.rgb();
                        for pos in crate::geometry::line(source_pos, dest) {
                            self.resources
                                .get_mut::<ParticleRequests>()
                                .unwrap()
                                .request(ParticleRequest {
                                    position: pos,
                                    tile: visible.tile_id,
                                    color: Color::RGB(r / 2, g / 2, b / 2),
                                    lifetime: Duration::from_millis(400),
                                });
                        }
                    }
                    self.world
                        .insert_one(who, MoveIntent(dest - source_pos))
                        .unwrap();
                } else {
                    warn!("Could not execute gap closer; no open spaces");
                }
            }
        }
    }
}

/// A `Dispatcher` is just a convenient wrapper around a bunch of
/// systems to setup/run them at once. It always runs them in
/// sequence, and always in the order they were pushed in.
#[derive(Debug)]
struct Dispatcher {
    systems: Vec<Box<dyn System + 'static>>,
}

impl Dispatcher {
    pub fn new() -> Self {
        Self { systems: vec![] }
    }

    pub fn push(&mut self, system: impl System + 'static) {
        self.systems.push(Box::new(system))
    }

    pub fn run(&mut self, world: &mut World, resources: &mut Resources) -> Result<()> {
        for system in self.systems.iter_mut() {
            system
                .run(world, resources)
                .with_context(|| format!("failed running {:?}", system))?;
        }
        Ok(())
    }

    pub fn setup(&mut self, world: &mut World, resources: &mut Resources) -> Result<()> {
        for system in self.systems.iter_mut() {
            system
                .setup(world, resources)
                .with_context(|| format!("failed setting up {:?}", system))?;
        }
        Ok(())
    }
}
