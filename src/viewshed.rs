//! Code for working with FOVs.

use crate::{geometry::*, map::Map};

#[derive(Debug, Clone)]
pub struct Viewshed {
    pub is_dirty: bool,
    pub range: i32,
    pub visible: Vec<WorldPoint>,
}

struct ShadowSpace;
type ShadowVector = euclid::Vector2D<i32, ShadowSpace>;

#[derive(Debug, Clone, Copy)]
struct Octant(u8);

impl Octant {
    fn transform(self, vector: ShadowVector) -> WorldVector {
        match self.0 {
            0 => WorldVector::new(vector.x, vector.y),
            1 => WorldVector::new(vector.y, vector.x),
            2 => WorldVector::new(vector.y, -vector.x),
            3 => WorldVector::new(vector.x, -vector.y),
            4 => WorldVector::new(-vector.x, -vector.y),
            5 => WorldVector::new(-vector.y, -vector.x),
            6 => WorldVector::new(-vector.y, vector.x),
            7 => WorldVector::new(-vector.x, vector.y),
            _ => panic!("bad octant {}", self.0),
        }
    }
}

impl Viewshed {
    pub fn new(range: i32) -> Self {
        assert!(range >= 0);
        Self {
            is_dirty: true,
            range,
            visible: vec![],
        }
    }

    pub fn compute(&mut self, map: &Map, center: WorldPoint) {
        self.visible = vec![];
        // This implements the shadow-line algorithm given in
        // https://journal.stuffwithstuff.com/2015/09/07/what-the-hero-sees/
        for octant in 0..8 {
            let octant = Octant(octant);
            self.compute_octant(octant, map, center);
        }
    }

    fn compute_octant(&mut self, octant: Octant, map: &Map, center: WorldPoint) {
        let mut shadow_line = ShadowLine::new();
        for row in 0..=self.range {
            let position = center + octant.transform(ShadowVector::new(row, 0));
            if !map.is_valid(position) {
                break;
            }
            for col in 0..=row {
                let position = position + octant.transform(ShadowVector::new(0, col));
                if !map.is_valid(position) {
                    break;
                }
                let shadow = Shadow::new(
                    (col as f32) / (row as f32 + 2.0),
                    (col as f32 + 1.0) / (row as f32 + 1.0),
                );
                if !shadow_line.contains(shadow) {
                    self.visible.push(position);
                    if map.is_opaque(position) {
                        shadow_line.add(shadow);
                    }
                }
            }
        }
    }
}

#[derive(Debug, Copy, Clone)]
struct Shadow {
    start: f32,
    end: f32,
}

impl Shadow {
    fn new(start: f32, end: f32) -> Self {
        assert!(
            start >= 0.0 && end <= 1.0 && start < end,
            "bad shadow interval {}-{}",
            start,
            end
        );
        Self { start, end }
    }

    fn contains(&self, other: Shadow) -> bool {
        self.start <= other.start && self.end >= other.end
    }
}

#[derive(Debug, Clone)]
struct ShadowLine {
    shadows: Vec<Shadow>,
}

impl ShadowLine {
    fn new() -> Self {
        Self { shadows: vec![] }
    }

    fn add(&mut self, shadow: Shadow) {
        let len = self.shadows.len();
        // If no overlaps, the shadow gets inserted immediately before
        // the first shadow that starts after it.
        let idx = self
            .shadows
            .iter()
            .position(|s| s.start >= shadow.start)
            .unwrap_or(len);
        let prev_overlap = idx > 0 && self.shadows[idx - 1].end > shadow.start;
        let next_overlap = idx < len && shadow.end > self.shadows[idx].start;
        match (prev_overlap, next_overlap) {
            // no overlaps
            (false, false) => self.shadows.insert(idx, shadow),
            // overlaps next, so just move next's start
            (false, true) => self.shadows[idx].start = shadow.start,
            // overlaps previous, so just move previous
            (true, false) => self.shadows[idx - 1].end = shadow.end,
            // overlaps both, so extend previous and delete next
            (true, true) => {
                self.shadows[idx - 1].end = self.shadows[idx].end;
                self.shadows.remove(idx);
            }
        }
    }

    /// Whether the given shadow is *fully* contained inside any
    /// shadow.
    fn contains(&self, shadow: Shadow) -> bool {
        self.shadows.iter().any(|s| s.contains(shadow))
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashSet;

    use super::*;
    use crate::map::TileType;

    fn make_empty_map(size: WorldSize) -> Map {
        let mut map = Map::new(size);
        for tile in map.tiles.iter_mut() {
            *tile = TileType::Floor;
        }
        map
    }

    fn check_visibility<P: Into<WorldPoint>>(
        map: &Map,
        center: WorldPoint,
        range: i32,
        expected: Vec<P>,
    ) {
        let mut viewshed = Viewshed::new(range);
        viewshed.compute(map, center);
        let expected: HashSet<WorldPoint> = expected.into_iter().map(Into::into).collect();
        let actual: HashSet<WorldPoint> = viewshed.visible.into_iter().collect();
        for x in 0..map.width() {
            for y in 0..map.height() {
                let point = WorldPoint::new(x, y);
                if expected.contains(&point) {
                    assert!(
                        actual.contains(&point),
                        "point {:?} not in viewshed, but should be",
                        point
                    );
                } else {
                    assert!(
                        !actual.contains(&point),
                        "point {:?} in viewshed, but shouldn't be",
                        point
                    )
                }
            }
        }
    }

    #[test]
    fn empty_map() {
        check_visibility(
            &make_empty_map(WorldSize::new(10, 10)),
            WorldPoint::new(5, 5),
            1,
            vec![
                (4, 4),
                (4, 5),
                (4, 6),
                (5, 4),
                (5, 5),
                (5, 6),
                (6, 4),
                (6, 5),
                (6, 6),
            ],
        );
    }

    #[test]
    fn full_map() {
        check_visibility(
            &Map::new(WorldSize::new(10, 10)),
            WorldPoint::new(5, 5),
            1,
            vec![(5, 5)],
        );
    }
}
