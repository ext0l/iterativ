#![allow(dead_code)]
//! Types used for geometry. In theory this could be a generic library, but I think it's easier to
//! just roll my own; it's not like performance is important here.

/// The in-game coordinate system. (0, 0) is the upper-left coordinate
/// of the map, and each tile is 1 by 1. Negative coordinates are not
/// meaningful (but we use i32 because negative *vectors* might be).
pub struct WorldSpace;

/// A position in worldspace.
pub type WorldPoint = euclid::Point2D<i32, WorldSpace>;

/// A displacement in worldspace.
pub type WorldVector = euclid::Vector2D<i32, WorldSpace>;

/// A rectangle in worldspace.
pub type WorldBox = euclid::Box2D<i32, WorldSpace>;

/// The size of something in worldspace.
pub type WorldSize = euclid::Size2D<i32, WorldSpace>;

/// The in-game coordinate system, translated so that the player is at
/// the center of it.
pub struct CameraSpace;

/// A position in cameraspace.
pub type CameraPoint = euclid::Point2D<i32, CameraSpace>;

/// A displacement in cameraspace.
pub type CameraVector = euclid::Vector2D<i32, CameraSpace>;

/// A rectangle in cameraspace.
pub type CameraBox = euclid::Box2D<i32, CameraSpace>;

/// The size of something in cameraspace.
pub type CameraSize = euclid::Size2D<i32, CameraSpace>;

/// The coordinate system of the containing window.
pub struct ScreenSpace;

/// A position in screenspace.
pub type ScreenPoint = euclid::Point2D<i32, ScreenSpace>;

/// A displacement in screenspace.
pub type ScreenVector = euclid::Vector2D<i32, ScreenSpace>;

/// A rectangle in screenspace.
pub type ScreenBox = euclid::Box2D<i32, ScreenSpace>;

/// The size of something in screenspace.
pub type ScreenSize = euclid::Size2D<i32, ScreenSpace>;

/// The coordinate system of the containing window.
pub struct GridSpace;

/// A position in gridspace.
pub type GridPoint = euclid::Point2D<i32, GridSpace>;

/// A displacement in gridspace.
pub type GridVector = euclid::Vector2D<i32, GridSpace>;

/// A rectangle in gridspace.
pub type GridBox = euclid::Box2D<i32, GridSpace>;

/// The size of something in gridspace.
pub type GridSize = euclid::Size2D<i32, GridSpace>;

/// A line from one point to another. Includes the start, but not the
/// end.
///
/// Implements the algorithm at
/// https://www.redblobgames.com/grids/line-drawing.html, which is
/// simpler than something like Bresenham's.
pub fn line<T: 'static>(
    from: euclid::Point2D<i32, T>,
    to: euclid::Point2D<i32, T>,
) -> impl Iterator<Item = euclid::Point2D<i32, T>> {
    let distance = i32::max((from.x - to.x).abs(), (from.y - to.y).abs());
    let from_f32 = from.to_f32();
    let to_f32 = to.to_f32();
    (0..distance).map(move |t| {
        from_f32
            .lerp(to_f32, (t as f32) / (distance as f32))
            .round()
            .to_i32()
    })
}
