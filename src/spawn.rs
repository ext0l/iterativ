use anyhow::Result;
use hecs::{Entity, World};
use rand::prelude::*;
use rand::seq::SliceRandom;

use crate::geometry::*;
use crate::{
    components::Position,
    map::{Map, TileType},
    raws::{RawId, Raws},
};
use log::debug;

pub fn room_spawns(rng: &mut impl Rng, map: &Map, room: &WorldBox) -> Vec<(WorldPoint, RawId)> {
    let mut floor = vec![];
    for y in room.min.y..room.max.y {
        for x in room.min.x..room.max.x {
            let point = WorldPoint::new(x, y);
            if map.tiles[map.idx(point)] == TileType::Floor {
                floor.push(point);
            }
        }
    }
    region_spawns(rng, map, &floor)
}

pub fn region_spawns(
    rng: &mut impl Rng,
    _map: &Map,
    region: &[WorldPoint],
) -> Vec<(WorldPoint, RawId)> {
    let spawn_count = usize::min(region.len(), rng.gen_range(1, 3));
    region
        .choose_multiple(rng, spawn_count)
        .map(|&point| (point, RawId("swarmer".to_string())))
        .collect()
}

pub fn spawn(raws: &Raws, world: &mut World, position: WorldPoint, name: &RawId) -> Result<Entity> {
    debug!("Spawning {} at {:?}", name, position);
    Ok(world.spawn(raws.create_entity(name)?.add(Position(position)).build()))
}
