use hecs::{Entity, World};
use resources::{CantGetResource, Resources};

use crate::geometry::WorldVector;
use crate::{engine::PlayerId, Action, Position};

/// A generic trait for objects to decide what they want to do next.
pub trait AI: Send + Sync + std::fmt::Debug {
    /// What does this entity want to do next? Returning `None` means that the entity has nothing
    /// that it wants to do at the moment.
    fn decide(&mut self, world: &World, resources: &Resources, me: Entity) -> Option<Action>;
}

/// This AI just moves towards the player as fast as possible.
#[derive(Copy, Clone, Debug)]
pub struct Swarm;

impl AI for Swarm {
    fn decide(&mut self, world: &World, resources: &Resources, me: Entity) -> Option<Action> {
        let player = resources.get::<PlayerId>().unwrap().0;
        let player_pos: Position = *world.get(player).unwrap();
        let my_pos: Position = *world.get(me).unwrap();
        let to_target = player_pos.0 - my_pos.0;
        if to_target.x.abs() <= 1 && to_target.y.abs() <= 1 {
            Some(Action::Attack { target: player })
        } else {
            Some(Action::Move {
                motion: WorldVector::new(to_target.x.signum(), to_target.y.signum()),
            })
        }
    }
}

/// This "AI" reads from the global PlayerAction resource. It's used so that the player character
/// can use the same AI system as the rest of the entities.
#[derive(Copy, Clone, Debug)]
pub struct PlayerAI;

/// This resources indicates the action the player wants to take. It's updated whenever the player
/// hits an input key, chooses an ability, etc.
#[derive(Debug)]
pub struct PlayerAction(pub Action);

impl AI for PlayerAI {
    fn decide(&mut self, _world: &World, resources: &Resources, _me: Entity) -> Option<Action> {
        match resources.get::<PlayerAction>() {
            Ok(action) => Some(action.0.clone()),
            Err(CantGetResource::InvalidBorrow(_)) => panic!("invalid borrow in PlayerAI::decide"),
            Err(CantGetResource::NoSuchResource(_)) => None,
        }
    }
}

#[derive(Debug)]
pub struct AIComponent(pub Box<dyn AI>);
