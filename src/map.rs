use hecs::Entity;
use itertools::Itertools;

use crate::geometry::*;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum TileType {
    Floor,
    Wall,
}

impl TileType {
    /// Whether the tile should be considered solid for pathfinding purposes.
    pub fn is_solid(&self) -> bool {
        match self {
            TileType::Floor => false,
            TileType::Wall => true,
        }
    }

    /// Whether the tile should be considered opaque for visibility purposes.
    pub fn is_opaque(&self) -> bool {
        match self {
            TileType::Floor => false,
            TileType::Wall => true,
        }
    }
}

#[derive(Debug)]
pub struct Map {
    size: WorldSize,
    // Implementation note: the vecs are stored in row-major order (so position 0 is top-left,
    // position 1 is to the right of that, etc).
    /// The tile that's located at each position of the map.
    pub tiles: Vec<TileType>,
    /// All entities located at a given position.
    pub entities: Vec<Vec<Entity>>,
    /// For each tile that has a blocking entity on it, this returns that entity.
    pub blockers: Vec<Option<Entity>>,
    /// What tiles are currently visible to the player?
    pub visible: Vec<bool>,
    /// What tiles has the player ever seen?
    pub revealed: Vec<bool>,
}

impl Map {
    pub fn new(size: WorldSize) -> Self {
        let area = size.area() as usize;
        Map {
            size,
            tiles: vec![TileType::Wall; area],
            blockers: vec![None; area],
            entities: vec![vec![]; area],
            visible: vec![false; area],
            revealed: vec![false; area],
        }
    }

    pub fn width(&self) -> i32 {
        self.size.width
    }

    pub fn height(&self) -> i32 {
        self.size.height
    }

    pub fn is_valid(&self, point: WorldPoint) -> bool {
        point.x >= 0 && point.y >= 0 && point.x < self.width() && point.y < self.height()
    }

    pub fn idx(&self, point: WorldPoint) -> usize {
        assert!(
            self.is_valid(point),
            "point {:?} not in map with size {:?}",
            point,
            self.size
        );
        (self.width() * point.y + point.x) as usize
    }

    /// Clears out all entities, including the cached tile blocking information. Does not modify
    /// the tiles themselves.
    pub fn clear_entities(&mut self) {
        self.blockers = vec![None; self.size.area() as usize];
        for entities in &mut self.entities {
            entities.clear();
        }
    }

    pub fn add_entity(&mut self, point: WorldPoint, entity: Entity, blocks: bool) {
        let idx = self.idx(point);
        if blocks {
            self.blockers[idx] = Some(entity)
        }
        self.entities[idx].push(entity);
    }

    /// Is movement onto this tile blocked?
    pub fn is_blocked(&self, point: WorldPoint) -> bool {
        let idx = self.idx(point);
        self.tiles[idx].is_solid() || self.blockers[idx].is_some()
    }

    /// Is this tile opaque for visibility calculations?
    pub fn is_opaque(&self, point: WorldPoint) -> bool {
        self.tiles[self.idx(point)].is_opaque()
    }

    /// If motion onto this tile is blocked by a specific entity, returns that entity. Note that
    /// this can return None even if is_blocked is true if the map itself blocks movement.
    pub fn blockers(&self, point: WorldPoint) -> Option<Entity> {
        self.blockers[self.idx(point)]
    }

    /// Marks off the given area as having the given tile type.
    pub fn add_rect(&mut self, tile: TileType, bounds: WorldBox) {
        for y in bounds.min.y..bounds.max.y {
            for x in bounds.min.x..bounds.max.x {
                let idx = self.idx(WorldPoint::new(x, y));
                self.tiles[idx] = tile;
            }
        }
    }

    /// Given a point, returns all points that have distance exactly 1
    /// away and are valid in the map. Does not filter based on
    /// whether the points are blocked.
    pub fn adjacent(&self, pos: WorldPoint) -> impl Iterator<Item = WorldPoint> + '_ {
        (-1..=1)
            .cartesian_product(-1..=1)
            .map(WorldVector::from)
            .filter(|v| *v != WorldVector::zero())
            .map(move |dv| pos + dv)
            .filter(move |adj| self.is_valid(*adj))
    }
}
