use anyhow::{anyhow, Result};
use enum_iterator::IntoEnumIterator;
use sdl2::{
    image::LoadTexture,
    render::{BlendMode, Texture},
};
use serde::Deserialize;
use std::collections::HashMap;
use std::path::Path;

use crate::geometry::*;

#[derive(Copy, Clone, PartialEq, Eq, Debug, Hash, IntoEnumIterator, Deserialize)]
pub enum TileId {
    Player,
    Grunt,
    Wall,
    Floor,
    Hidden,
    OutOfBounds,
    HitParticle,
    Reticule,
}

impl TileId {
    fn filename(&self) -> &'static str {
        match self {
            TileId::Player => "player",
            TileId::Grunt => "grunt",
            TileId::Wall => "wall",
            TileId::Floor => "floor",
            TileId::Hidden => "hidden",
            TileId::OutOfBounds => "out_of_bounds",
            TileId::HitParticle => "hit_particle",
            TileId::Reticule => "reticule",
        }
    }
}

/// Stores all the game's textures. This is passed around wrapped in
/// an `Rc<RefCell<_>>`, usually, so that we can have interior
/// mutability which allows us to set blend modes and the like on the textures.
pub struct Tiles<'tex> {
    size: ScreenSize,
    // This hashmap is guaranteed to have an entry for each TileId.
    textures: HashMap<TileId, Texture<'tex>>,
}

impl<'tex> Tiles<'tex> {
    /// Loads all the tiles in the given directory. This function checks that all fo the tiles have
    /// the given size.
    pub fn new<P: AsRef<Path>>(
        creator: &'tex impl LoadTexture,
        directory: P,
        size: ScreenSize,
    ) -> Result<Self> {
        let mut textures = HashMap::new();
        for tile in TileId::into_enum_iter() {
            let filename = directory
                .as_ref()
                .join(tile.filename())
                .with_extension("png");
            let mut tex = creator
                .load_texture(filename)
                .map_err(|e| anyhow!("SDL error: {}", e))?;
            tex.set_blend_mode(BlendMode::Blend);
            let query = tex.query();
            assert_eq!(query.width, size.width as u32);
            assert_eq!(query.height, size.height as u32);
            textures.insert(tile, tex);
        }
        Ok(Self { size, textures })
    }

    pub fn texture(&self, tile: TileId) -> &Texture<'tex> {
        &self.textures[&tile]
    }

    /// Callers of this function should not mutate the texture's
    /// backing data, just things like the alpha.
    pub fn texture_mut(&mut self, tile: TileId) -> &mut Texture<'tex> {
        self.textures.get_mut(&tile).unwrap()
    }

    pub fn size(&self) -> ScreenSize {
        self.size
    }
}
