mod bsp;
pub use bsp::*;
mod spawn;
pub use spawn::*;

use crate::geometry::*;
use crate::map::Map;

/// A struct passed around between map generators.
#[derive(Debug)]
pub struct Blueprint {
    pub map: Map,
    /// If this is set, then it's a list of rooms.
    pub rooms: Option<Vec<WorldBox>>,
    pub spawn_point: Option<WorldPoint>,
}

/// Methods of generating an initial map.
pub trait MapBuilder {
    fn build(self) -> Blueprint;
}
