//! Code for spawning entities in maps.
use hecs::EntityBuilder;
use sdl2::pixels::Color;

use crate::components::*;

use crate::{ai::*, tiles::TileId};

/// Creates the player entity, and inserts it into the world. Does not
/// set its position.
pub fn create_player() -> EntityBuilder {
    let mut builder = EntityBuilder::new();
    builder
        .add(Name {
            name: "you".to_string(),
        })
        .add(AIComponent(Box::new(PlayerAI)))
        .add(IsPlayer)
        .add(Visible {
            tile_id: TileId::Player,
            color: Color::RGBA(0x00, 0xc1, 0xc4, 0xff),
            alpha: 1.0,
        })
        .add(CombatStats {
            max_hp: 20,
            hp: 20,
            attack: 3,
        })
        .add(Initiative::new(10))
        .add(BlocksMovement)
        .add(Viewshed::new(9));
    builder
}
