use crate::geometry::*;
use crate::map::*;
use crate::mapgen::*;
use rand::prelude::*;
use rand::Rng;

/// How are the node's children partitioned? This indicates the
/// direction of the split; i.e., horizontal means the parts are like
/// X|X.
#[derive(Copy, Clone, Debug)]
enum Axis {
    Horizontal,
    Vertical,
}

#[derive(Clone, Debug)]
enum Contents {
    /// This is a leaf node containing a room. Note that the room's
    /// bounds are not the same as the node's bounds.
    Room(WorldBox),
    /// A branch node containing a left and a right child.
    Branch {
        axis: Axis,
        children: (Box<Node>, Box<Node>),
    },
}

#[derive(Clone, Debug)]
struct Node {
    bounds: WorldBox,
    contents: Contents,
}

#[derive(Debug)]
pub struct BSPConfig {
    pub size: WorldSize,
    /// The width of a node inside the final BSP tree. Note that rooms will be *smaller* than this.
    pub min_node_size: i32,
}

/// Splits the box in two at the coordinate.
fn split_box(bounds: WorldBox, direction: Axis, at: i32) -> (WorldBox, WorldBox) {
    match direction {
        Axis::Horizontal => {
            let left = WorldBox::new(bounds.min, WorldPoint::new(bounds.min.x + at, bounds.max.y));
            let right = WorldBox::new(
                WorldPoint::new(bounds.min.x + at + 1, bounds.min.y),
                bounds.max,
            );
            (left, right)
        }
        Axis::Vertical => {
            let top = WorldBox::new(bounds.min, WorldPoint::new(bounds.max.x, bounds.min.y + at));
            let bottom = WorldBox::new(
                WorldPoint::new(bounds.min.x, bounds.min.y + at + 1),
                bounds.max,
            );
            (top, bottom)
        }
    }
}

#[derive(Debug)]
pub struct BSPGenerator<R: Rng> {
    config: BSPConfig,
    blueprint: Blueprint,
    rng: R,
}

impl<R: Rng> BSPGenerator<R> {
    pub fn new(rng: R, config: BSPConfig) -> Self {
        let blueprint = Blueprint {
            map: Map::new(config.size),
            rooms: Some(vec![]),
            spawn_point: None,
        };
        Self {
            config,
            blueprint,
            rng,
        }
    }

    /// Determines whether to split the node, then either generates a
    /// room inside it or splits it into subnodes and calls
    /// `generate` recursively.
    fn generate(&mut self, bounds: WorldBox) -> Node {
        if let Some((axis, at)) = self.choose_split(&bounds) {
            let splits = split_box(bounds, axis, at);
            let first = self.generate(splits.0);
            let second = self.generate(splits.1);
            Node {
                bounds,
                contents: Contents::Branch {
                    axis,
                    children: (Box::new(first), Box::new(second)),
                },
            }
        } else {
            let room = WorldBox::new(
                WorldPoint::new(
                    bounds.min.x + self.rng.gen_range(0, bounds.width() / 3),
                    bounds.min.y + self.rng.gen_range(0, bounds.height() / 3),
                ),
                WorldPoint::new(
                    bounds.max.x - self.rng.gen_range(0, bounds.width() / 3),
                    bounds.max.y - self.rng.gen_range(0, bounds.height() / 3),
                ),
            );
            Node {
                bounds,
                contents: Contents::Room(room),
            }
        }
    }

    /// Split a length so that both children will have at least the
    /// minimum node size. Returns `None` if this is impossible.
    fn split_length(&mut self, length: i32) -> Option<i32> {
        // off-by-one stuff: if the outer node has size 7, and the
        // minimum node size is 2, then we can only split it *exactly*
        // in the middle at 3, since we get something like XOOXOOX
        let low = self.config.min_node_size + 1;
        let high = length - self.config.min_node_size - 1;
        if low >= high {
            None
        } else {
            Some(self.rng.gen_range(low, high))
        }
    }

    /// Decide how to split the given box.
    fn choose_split(&mut self, bounds: &WorldBox) -> Option<(Axis, i32)> {
        let horizontal = self.split_length(bounds.width());
        let vertical = self.split_length(bounds.height());
        match (horizontal, vertical) {
            (None, None) => None,
            (Some(at), None) => Some((Axis::Horizontal, at)),
            (None, Some(at)) => Some((Axis::Vertical, at)),
            (Some(horizontal), Some(vertical)) => Some({
                let aspect_ratio = (bounds.width() as f64) / (bounds.height() as f64);
                if aspect_ratio > 2.0 {
                    (Axis::Horizontal, horizontal)
                } else if aspect_ratio < 1.0 / 2.0 {
                    (Axis::Vertical, vertical)
                } else if self.rng.gen() {
                    (Axis::Horizontal, horizontal)
                } else {
                    (Axis::Vertical, vertical)
                }
            }),
        }
    }

    fn fill_map(&mut self, node: &Node) {
        match &node.contents {
            Contents::Room(bounds) => {
                self.blueprint.map.add_rect(TileType::Floor, *bounds);
                self.blueprint.rooms.as_mut().unwrap().push(*bounds);
            }
            Contents::Branch { children, .. } => {
                self.fill_map(&*children.0);
                self.fill_map(&*children.1);
            }
        }
    }

    fn fill_corridors(&mut self, node: &Node) {
        if let Contents::Branch { children, .. } = &node.contents {
            let corridor = WorldBox::new(
                children.0.bounds.center(),
                children.1.bounds.center() + WorldVector::new(1, 1),
            );
            self.blueprint.map.add_rect(TileType::Floor, corridor);
            self.fill_corridors(&*children.0);
            self.fill_corridors(&*children.1);
        }
    }
}

impl<R: Rng> MapBuilder for BSPGenerator<R> {
    fn build(mut self) -> Blueprint {
        // Deflate the room by 1 on each side so that it will always
        // generate walls around the edges.
        let root = self.generate(WorldBox::from_size(self.config.size).inflate(-1, -1));
        self.fill_map(&root);
        self.fill_corridors(&root);
        let spawn_room = self.blueprint.rooms.as_ref().unwrap().choose(&mut self.rng);
        self.blueprint.spawn_point = Some(spawn_room.unwrap().center());
        self.blueprint
    }
}
