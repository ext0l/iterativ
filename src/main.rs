mod ability;
mod ai;
mod components;
mod engine;
mod event_log;
mod geometry;
mod gfx;
mod map;
mod mapgen;
mod raws;
mod spawn;
mod systems;
mod tiles;
mod viewshed;

use ability::Ability;
use anyhow::{Context, Result};

use crate::components::*;
use crate::engine::*;
use crate::geometry::*;
use crate::gfx::Gfx;

use crate::mapgen::*;
use crate::tiles::*;

use raws::{RawId, Raws};
use sdl2::{event::Event, keyboard::Keycode, pixels::Color, render::WindowCanvas};
use std::{cell::RefCell, convert::TryInto, rc::Rc};

const WIDTH: i32 = 80;
const HEIGHT: i32 = 40;
const TILE_SIZE: i32 = 16;

struct Iterativ<'a> {
    gfx: Gfx<'a>,
    state: Engine,
}

impl<'a> Iterativ<'a> {
    fn new(gfx: Gfx<'a>) -> Result<Self> {
        let mut state = Engine::new().context("couldn't create Engine")?;
        let raws = Raws::load()?;

        let generator = BSPGenerator::new(
            rand::thread_rng(),
            BSPConfig {
                size: WorldSize::new(WIDTH, HEIGHT),
                min_node_size: 8,
            },
        );
        let blueprint = generator.build();
        let mut spawns = vec![];
        let mut rng = rand::thread_rng();
        if let Some(rooms) = &blueprint.rooms {
            for room in rooms.iter() {
                spawns.extend(spawn::room_spawns(&mut rng, &blueprint.map, room).into_iter())
            }
        }
        for (position, name) in spawns.into_iter() {
            spawn::spawn(&raws, &mut state.world, position, &name)
                .context("spawning in map generation")?;
        }
        state.resources.insert(blueprint.map);

        let player = state.world.spawn(
            create_player()
                .add(Position(blueprint.spawn_point.unwrap()))
                .build(),
        );
        state.resources.insert(PlayerId(player));

        state.world.spawn(
            raws.create_entity(&RawId("swarmer".into()))?
                .add(Position((10, 10).into()))
                .build(),
        );
        state.resources.insert(event_log::EventLog::new());
        Ok(Iterativ { gfx, state })
    }

    fn draw(&mut self, canvas: &mut WindowCanvas) -> Result<()> {
        canvas.set_draw_color(Color::BLACK);
        canvas.clear();
        let player = self.state.player();
        let player_position = self
            .state
            .world
            .get::<Position>(player)
            .expect("couldn't find player position")
            .0;
        self.gfx.set_camera_center(player_position);
        self.gfx.draw(canvas, &self.state)?;
        self.state.mark_layers_clean();
        Ok(())
    }

    pub fn event(&mut self, event: &Event) -> Result<()> {
        if *self.state.loop_state() == LoopState::GameOver {
            // disable game input in game over
            return Ok(());
        }
        if let Event::KeyDown {
            keycode: Some(keycode),
            ..
        } = event
        {
            match keycode {
                Keycode::H => self.state.set_player_action(Action::Move {
                    motion: (-1, 0).into(),
                }),
                Keycode::J => self.state.set_player_action(Action::Move {
                    motion: (0, 1).into(),
                }),
                Keycode::K => self.state.set_player_action(Action::Move {
                    motion: (0, -1).into(),
                }),
                Keycode::L => self.state.set_player_action(Action::Move {
                    motion: (1, 0).into(),
                }),
                Keycode::Y => self.state.set_player_action(Action::Move {
                    motion: (-1, -1).into(),
                }),
                Keycode::U => self.state.set_player_action(Action::Move {
                    motion: (1, -1).into(),
                }),
                Keycode::B => self.state.set_player_action(Action::Move {
                    motion: (-1, 1).into(),
                }),
                Keycode::N => self.state.set_player_action(Action::Move {
                    motion: (1, 1).into(),
                }),
                Keycode::Q => self.state.start_targeting(Ability::GapCloser),
                Keycode::F1 => {
                    let options = self.gfx.options_mut();
                    options.show_out_of_bounds = !options.show_out_of_bounds;
                }
                Keycode::Tab => {
                    if matches!(*self.state.loop_state(), LoopState::Targeting { .. }) {
                        self.state.next_target()
                    }
                }
                Keycode::Return => {
                    if matches!(*self.state.loop_state(), LoopState::Targeting { .. }) {
                        self.state.select_ability_target();
                    }
                }
                Keycode::Escape => {
                    if matches!(*self.state.loop_state(), LoopState::Targeting { .. }) {
                        self.state.cancel_targeting();
                    }
                }

                _ => (),
            }
        }
        Ok(())
    }

    fn update(&mut self) -> Result<()> {
        if *self.state.loop_state() != LoopState::GameOver {
            self.state.tick().context("game tick error")?;
            self.state.world.flush();
        }
        Ok(())
    }
}

fn main() {
    env_logger::init();

    let sdl_context = sdl2::init().expect("failed to init sdl context");
    let video = sdl_context.video().expect("failed to get video system");

    let map_region = ScreenBox::new(ScreenPoint::zero(), ScreenPoint::new(960, 640));
    let log_region = ScreenBox::new(ScreenPoint::new(0, 640), ScreenPoint::new(960, 800));
    let sidebar_region = ScreenBox::new(ScreenPoint::new(960, 0), ScreenPoint::new(1280, 800));
    let window_box = map_region.union(&log_region).union(&sidebar_region);

    let window = video
        .window(
            "iterativ",
            window_box.width().try_into().unwrap(),
            window_box.height().try_into().unwrap(),
        )
        .position_centered()
        .build()
        .expect("failed to build window");
    let mut canvas = window
        .into_canvas()
        .present_vsync()
        .build()
        .expect("failed to get canvas");
    let texture_creator = canvas.texture_creator();
    let tiles = Tiles::new(
        &texture_creator,
        "static/sprites",
        ScreenSize::new(TILE_SIZE, TILE_SIZE),
    )
    .unwrap();
    let ttf_ctx = sdl2::ttf::init().expect("failed to get font");
    let font = ttf_ctx.load_font("static/meslo.ttf", 16).unwrap();

    let mut event_pump = sdl_context
        .event_pump()
        .expect("failed to start event pump");

    let tiles = Rc::new(RefCell::new(tiles));
    let gfx = Gfx::new(
        &texture_creator,
        tiles,
        map_region,
        log_region,
        sidebar_region,
        &font,
    );

    let mut app = Iterativ::new(gfx).expect("failed to init app");

    loop {
        for event in event_pump.poll_iter() {
            if let Event::Quit { .. } = event {
                return;
            }
            app.event(&event).unwrap();
        }
        if event_pump.mouse_state().left() {
            println!("click!")
        }
        app.update().unwrap();
        app.draw(&mut canvas).unwrap();
    }
}
