//! Struct definitions for working with the raw JSON representations
//! of monsters, etc.

use anyhow::{bail, Result};
use hecs::EntityBuilder;
use sdl2::pixels::Color;
use serde::Deserialize;

use crate::components::*;
use crate::{ai::*, tiles::TileId};
use std::collections::HashMap;
use std::fmt;

#[derive(Debug, Clone, Deserialize, PartialOrd, Ord, PartialEq, Eq, Hash)]
pub struct RawId(pub String);

impl fmt::Display for RawId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "[{}]", self.0)
    }
}

pub struct Raws {
    /// Maps from the mob's ID to its string.
    pub mobs: HashMap<RawId, Mob>,
}

impl Raws {
    pub fn load() -> Result<Self> {
        let mob_vec: Vec<Mob> = serde_json::from_str(include_str!("../static/raw/mobs.json"))?;
        let mut mobs = HashMap::new();
        for mob in mob_vec.into_iter() {
            assert!(!mobs.contains_key(&mob.id), "duplicate mob id {}", mob.id);
            mobs.insert(mob.id.clone(), mob);
        }
        Ok(Raws { mobs })
    }

    pub fn create_entity(&self, id: &RawId) -> Result<EntityBuilder> {
        if self.mobs.contains_key(id) {
            self.create_mob(&self.mobs[id])
        } else {
            bail!("Unable to find entity named {}", id)
        }
    }

    fn create_mob(&self, mob: &Mob) -> Result<EntityBuilder> {
        let mut builder = EntityBuilder::new();
        builder
            .add(Initiative::new(mob.stats.initiative))
            .add(Name {
                name: mob.name.clone(),
            })
            .add(CombatStats {
                max_hp: mob.stats.max_hp,
                hp: mob.stats.max_hp,
                attack: mob.stats.attack,
            })
            .add(mob.ai.as_component())
            .add(Visible {
                tile_id: mob.sprite.tile_id,
                color: Color::RGBA(0xe3, 0x83, 0x14, 0xff),
                alpha: 1.0,
            })
            .add(BlocksMovement);
        Ok(builder)
    }
}

#[derive(Debug, Deserialize, Clone)]
pub struct Mob {
    id: RawId,
    name: String,
    stats: MobStats,
    ai: AIEnum,
    sprite: Sprite,
}

#[derive(Debug, Deserialize, Clone)]
pub struct MobStats {
    max_hp: i32,
    attack: i32,
    initiative: i32,
}

#[derive(Debug, Deserialize, Clone)]
pub enum AIEnum {
    Swarm,
}

impl AIEnum {
    fn as_component(&self) -> AIComponent {
        AIComponent(match self {
            AIEnum::Swarm => Box::new(Swarm),
        })
    }
}

#[derive(Debug, Deserialize, Clone)]
pub struct Sprite {
    tile_id: TileId,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn check_load() {
        Raws::load().unwrap();
    }
}
