use anyhow::Result;
use hecs::World;
use log::debug;
use resources::Resources;

use crate::components::*;
use crate::engine::LoopState;

use super::System;

#[derive(Debug)]
pub struct InitiativeSystem;

impl System for InitiativeSystem {
    fn run(&mut self, world: &mut World, resources: &mut Resources) -> Result<()> {
        // Only run if we're looping *and* nobody is ready.
        if !(*resources.get::<LoopState>().unwrap() == LoopState::Looping
            && world.query_mut::<&Ready>().into_iter().next().is_none())
        {
            return Ok(());
        }

        let ready: Vec<_> = world
            .query_mut::<&mut Initiative>()
            .into_iter()
            .filter_map(|(entity, initiative)| {
                if initiative.tick() {
                    Some(entity)
                } else {
                    None
                }
            })
            .collect();

        for entity in ready.into_iter() {
            world
                .insert_one(entity, Ready)
                .expect("can't set Ready component");
            debug!("entity {:?} is ready", entity);
        }
        Ok(())
    }
}
