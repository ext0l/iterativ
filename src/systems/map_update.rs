use anyhow::Result;
use hecs::World;
use resources::Resources;

use crate::components::*;
use crate::map::Map;

use super::System;

#[derive(Debug)]
pub struct MapUpdateSystem;

impl System for MapUpdateSystem {
    fn run(&mut self, world: &mut World, resources: &mut Resources) -> Result<()> {
        let mut map = resources.get_mut::<Map>()?;
        map.clear_entities();
        for (entity, (position, blocked)) in
            world.query_mut::<(&Position, Option<&BlocksMovement>)>()
        {
            map.add_entity(position.0, entity, blocked.is_some());
        }
        Ok(())
    }
}
