mod initiative;
use std::fmt::Debug;

pub use initiative::InitiativeSystem;
mod map_update;
pub use map_update::MapUpdateSystem;
mod attack;
pub use attack::AttackSystem;
mod movement;
pub use movement::MovementSystem;
mod damage;
pub use damage::DamageSystem;
mod death;
pub use death::DeathSystem;
mod viewshed;
pub use viewshed::ViewshedSystem;
mod particle;
pub use particle::{Particle, ParticleRequest, ParticleRequests, ParticleSystem};

use anyhow::Result;
use hecs::{Component, Entity, World};
use resources::Resources;

pub trait System: Debug {
    /// Runs over the world.
    fn run(&mut self, world: &mut World, resources: &mut Resources) -> Result<()>;

    /// Initial setup. Called exactly once.
    #[allow(unused_variables)]
    fn setup(&mut self, world: &mut World, resources: &mut Resources) -> Result<()> {
        Ok(())
    }
}

pub fn remove_all<T: Component>(world: &mut World) -> Result<()> {
    let to_remove: Vec<Entity> = world
        .query_mut::<&T>()
        .into_iter()
        .map(|(entity, _)| entity)
        .collect();
    for entity in to_remove.into_iter() {
        world.remove_one::<T>(entity)?;
    }
    Ok(())
}
