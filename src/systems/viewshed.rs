use anyhow::{Context, Result};
use hecs::World;
use resources::Resources;

use crate::components::*;
use crate::engine::DirtyLayers;
use crate::map::Map;

use super::System;

/// Updates viewsheds for everything that has one.
#[derive(Debug)]
pub struct ViewshedSystem;

impl System for ViewshedSystem {
    fn run(&mut self, world: &mut World, resources: &mut Resources) -> Result<()> {
        let mut map = resources.get_mut::<Map>().context("couldn't get map")?;
        let mut layers = resources
            .get_mut::<DirtyLayers>()
            .context("couldn't get DirtyLayers")?;
        for (_, (mut viewshed, position, is_player)) in
            world.query_mut::<(&mut Viewshed, &Position, Option<&IsPlayer>)>()
        {
            {
                if !viewshed.is_dirty {
                    continue;
                }
                viewshed.compute(&map, position.0);
                viewshed.is_dirty = false;
                if is_player.is_some() {
                    layers.map = true;
                    for vis in map.visible.iter_mut() {
                        *vis = false;
                    }
                    for vis in viewshed.visible.iter() {
                        let idx = map.idx(*vis);
                        map.visible[idx] = true;
                        map.revealed[idx] = true;
                    }
                }
            }
        }
        Ok(())
    }
}
