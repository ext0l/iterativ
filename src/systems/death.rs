use anyhow::{Context, Result};
use hecs::{Entity, World};
use resources::Resources;

use crate::components::*;
use crate::engine::LoopState;
use crate::event_log::{Event, EventLog};

use super::System;

/// Reaps dead entities.
#[derive(Debug)]
pub struct DeathSystem;

impl System for DeathSystem {
    fn run(&mut self, world: &mut World, resources: &mut Resources) -> Result<()> {
        let mut to_die: Vec<Entity> = Vec::new();
        for (entity, (stats, is_player)) in world.query_mut::<(&CombatStats, Option<&IsPlayer>)>() {
            if stats.hp <= 0 {
                to_die.push(entity);
                if is_player.is_some() {
                    resources.insert(LoopState::GameOver);
                }
            }
        }
        for dead in to_die {
            resources
                .get_mut::<EventLog>()
                .context("couldn't get event log")?
                .log(Event::Death { who: dead }.format(world));
            world.despawn(dead).context("couldn't despawn entity")?;
        }
        Ok(())
    }
}
