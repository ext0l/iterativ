use std::time::Duration;

use anyhow::{Context, Result};
use hecs::World;
use resources::Resources;
use sdl2::pixels::Color;

use crate::{components::*, tiles::TileId};
use crate::{engine::FrameDuration, geometry::*};

use super::System;

#[derive(Copy, Clone, Debug)]
pub struct Particle {
    lifetime: Duration,
    elapsed: Duration,
}

impl Particle {
    pub fn new(lifetime: Duration) -> Self {
        Self {
            lifetime,
            elapsed: Duration::from_secs(0),
        }
    }

    /// Alpha (in 0.0 to 1.0) that this should be drawn with.
    pub fn alpha(&self) -> f32 {
        let ratio = 1.0 - self.elapsed.as_secs_f32() / self.lifetime.as_secs_f32();
        if ratio.is_nan() {
            1.0
        } else {
            ratio.clamp(0.0, 1.0)
        }
    }

    pub fn tick(&mut self, dt: Duration) {
        self.elapsed += dt;
    }

    pub fn expired(&self) -> bool {
        self.elapsed > self.lifetime
    }
}

#[derive(Copy, Clone, Debug)]
pub struct ParticleRequest {
    pub position: WorldPoint,
    pub tile: TileId,
    pub color: Color,
    pub lifetime: Duration,
}

pub struct ParticleRequests(Vec<ParticleRequest>);

impl ParticleRequests {
    pub fn new() -> Self {
        Self(vec![])
    }

    pub fn request(&mut self, request: ParticleRequest) {
        self.0.push(request);
    }
}

/// Reaps particles whose lifetimes have expired and spawns new
/// particles in accordance with the `ParticleRequest`s.
#[derive(Debug)]
pub struct ParticleSystem;

impl ParticleSystem {
    /// Reaps the old particles.
    pub fn reap(&mut self, world: &mut World) {
        let mut to_reap = vec![];
        for (entity, particle) in world.query::<&Particle>().iter() {
            if particle.expired() {
                to_reap.push(entity);
            }
        }
        for entity in to_reap.into_iter() {
            world
                .despawn(entity)
                .expect(&format!("error reaping expired particle {:?}", entity));
        }
    }

    /// Processes the new particle requests. {
    pub fn build_particles(
        &mut self,
        world: &mut World,
        requests: impl Iterator<Item = ParticleRequest>,
    ) {
        world
            .spawn_batch(requests.map(|request| {
                (
                    (Position(request.position)),
                    (Visible {
                        tile_id: request.tile,
                        color: request.color,
                        alpha: 1.0,
                    }),
                    (Particle::new(request.lifetime)),
                )
            }))
            .last();
    }
}

impl System for ParticleSystem {
    fn run(&mut self, world: &mut World, resources: &mut Resources) -> Result<()> {
        let frame_duration = resources
            .get::<FrameDuration>()
            .context("couldn't get frame duration")?;

        self.reap(world);
        let mut requests = resources.get_mut::<ParticleRequests>().unwrap();
        self.build_particles(world, requests.0.drain(..));

        for (_, (particle, visible)) in world.query::<(&Particle, &mut Visible)>().iter() {
            visible.alpha = particle.alpha();
        }

        for (_, particle) in world.query::<&mut Particle>().iter() {
            particle.tick(frame_duration.0);
        }

        Ok(())
    }

    fn setup(&mut self, _world: &mut World, resources: &mut Resources) -> Result<()> {
        resources.insert(ParticleRequests::new());
        Ok(())
    }
}
