use anyhow::Result;
use hecs::World;
use resources::Resources;

use crate::components::*;

use super::System;

#[derive(Debug)]
pub struct DamageSystem;

impl System for DamageSystem {
    fn run(&mut self, world: &mut World, _resources: &mut Resources) -> Result<()> {
        for (_, (stats, queue)) in world.query_mut::<(&mut CombatStats, &mut QueuedDamage)>() {
            stats.hp -= queue.0.drain(..).sum::<i32>();
        }
        Ok(())
    }
}
