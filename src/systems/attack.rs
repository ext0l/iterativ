use anyhow::Result;
use hecs::World;
use resources::Resources;
use sdl2::pixels::Color;
use std::time::Duration;

use crate::components::*;
use crate::event_log::{Event, EventLog};
use crate::systems::{ParticleRequest, ParticleRequests};
use crate::tiles::TileId;

use super::System;

#[derive(Debug)]
pub struct AttackSystem;

impl System for AttackSystem {
    fn run(&mut self, world: &mut World, resources: &mut Resources) -> Result<()> {
        let mut event_log = resources.get_mut::<EventLog>()?;
        let mut particle_requests = resources.get_mut::<ParticleRequests>()?;
        let mut queue = vec![];
        for (entity, (intent, stats)) in world.query::<(&AttackIntent, &CombatStats)>().iter() {
            queue.push((intent.target, stats.attack));
            event_log.log(
                Event::Damage {
                    from: entity,
                    to: intent.target,
                    amount: stats.attack,
                }
                .format(world),
            );
            if let Ok(position) = world.get::<Position>(intent.target) {
                particle_requests.request(ParticleRequest {
                    position: position.0,
                    tile: TileId::HitParticle,
                    color: Color::RED,
                    lifetime: Duration::from_millis(400),
                });
            }
        }
        for (target, amount) in queue.into_iter() {
            QueuedDamage::add(world, target, amount);
        }
        super::remove_all::<AttackIntent>(world)?;
        Ok(())
    }
}
