use anyhow::{Context, Result};
use hecs::{Entity, World};
use resources::Resources;

use crate::components::*;
use crate::map::Map;

use super::System;

#[derive(Debug)]
pub struct MovementSystem;

impl System for MovementSystem {
    fn run(&mut self, world: &mut World, resources: &mut Resources) -> Result<()> {
        let map = resources.get::<Map>().context("couldn't get map")?;
        for (_, (intent, position, viewshed)) in
            world.query_mut::<(&MoveIntent, &mut Position, Option<&mut Viewshed>)>()
        {
            let new_pos = position.0 + intent.0;
            if !map.is_blocked(new_pos) {
                position.0 = new_pos;
                if let Some(viewshed) = viewshed {
                    viewshed.is_dirty = true;
                }
            }
        }

        let to_remove: Vec<Entity> = world
            .query_mut::<&MoveIntent>()
            .into_iter()
            .map(|(entity, _)| entity)
            .collect();
        for entity in to_remove.into_iter() {
            world
                .remove_one::<MoveIntent>(entity)
                .with_context(|| format!("failed to remove move intent for {:?}", entity))?;
        }
        Ok(())
    }
}
