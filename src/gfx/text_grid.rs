use anyhow::{anyhow, Context, Result};
use std::{
    cell::RefCell,
    collections::{hash_map::Entry, HashMap},
    rc::Rc,
};

use sdl2::{
    pixels::Color, pixels::PixelFormatEnum, render::BlendMode, render::Texture,
    render::TextureCreator, render::WindowCanvas, ttf::Font, video::WindowContext,
};

use crate::geometry::{GridPoint, GridSize, ScreenSize};

use super::layer::RenderTargetGuard;

#[derive(Debug, Copy, Clone)]
pub struct GridCell {
    ch: char,
    color: Color,
}

pub struct TextGrid<'a> {
    size: GridSize,
    font: &'a Font<'a, 'static>,
    font_size: ScreenSize,
    font_cache: RefCell<HashMap<char, Texture<'a>>>,
    /// first index is row, second is column.
    grid: Vec<Option<GridCell>>,
    texture: Rc<Texture<'a>>,
    texture_creator: &'a TextureCreator<WindowContext>,
}

impl<'a> TextGrid<'a> {
    pub fn new(
        screen_size: ScreenSize,
        texture_creator: &'a TextureCreator<WindowContext>,
        font: &'a Font<'a, 'static>,
    ) -> Result<Self> {
        let rendered = font.render_char('a').blended(Color::WHITE)?;
        let font_size = ScreenSize::new(rendered.width() as i32, rendered.height() as i32);
        let size = GridSize::new(
            screen_size.width / font_size.width,
            screen_size.height / font_size.height,
        );
        let grid = vec![None; (size.width * size.height) as usize];
        let texture = texture_creator.create_texture_target(
            PixelFormatEnum::ARGB8888,
            screen_size.width as u32,
            screen_size.height as u32,
        )?;
        Ok(Self {
            size,
            font,
            font_size,
            font_cache: RefCell::new(HashMap::new()),
            grid,
            texture: Rc::new(texture),
            texture_creator,
        })
    }

    pub fn clear(&mut self) {
        for cell in self.grid.iter_mut() {
            *cell = None;
        }
    }

    /// Draws text starting at the given location. Returns the point after the end.
    pub fn draw_text(&mut self, start: GridPoint, text: &str, color: Color) -> GridPoint {
        let mut point = start;
        for ch in text.chars() {
            point.x += 1;
            *self.cell_mut(point) = Some(GridCell { ch, color });
        }
        point
    }

    pub fn cell_mut(&mut self, point: GridPoint) -> &mut Option<GridCell> {
        assert!(point.x >= 0);
        assert!(point.y >= 0);
        assert!(point.x < self.size.width);
        assert!(point.y < self.size.height);
        let idx = point.y * self.size.width + point.x;
        &mut self.grid[idx as usize]
    }

    pub fn render(&mut self, canvas: &mut WindowCanvas) -> Result<&Texture<'a>> {
        let mut font_cache = self.font_cache.borrow_mut();
        let mut guard = RenderTargetGuard::new(canvas, self.texture.clone())?;
        let canvas = guard.canvas();
        canvas.set_draw_color(Color::RGBA(0, 0, 0, 0));
        canvas.clear();
        for (idx, cell) in self.grid.iter().enumerate() {
            let point = self.from_idx(idx);
            if let Some(cell) = cell {
                let char_tex = self
                    .render_symbol(&mut *font_cache, cell.ch)
                    .with_context(|| format!("couldn't render symbol '{}'", cell.ch))?;
                let dest_rect = sdl2::rect::Rect::new(
                    point.x * self.font_size.width,
                    point.y * self.font_size.height,
                    self.font_size.width as u32,
                    self.font_size.height as u32,
                );
                char_tex.set_color_mod(cell.color.r, cell.color.g, cell.color.b);
                canvas
                    .copy(char_tex, None, dest_rect)
                    .map_err(|e| anyhow!("could not copy character at {:?}: {}", point, e))?;
            }
        }

        Ok(&self.texture)
    }

    fn from_idx(&self, idx: usize) -> GridPoint {
        GridPoint::new(
            (idx as i32) % self.size.width,
            (idx as i32) / self.size.width,
        )
    }

    fn render_symbol<'f>(
        &self,
        font_cache: &'f mut HashMap<char, Texture<'a>>,
        ch: char,
    ) -> Result<&'f mut Texture<'a>> {
        match font_cache.entry(ch) {
            Entry::Occupied(entry) => Ok(entry.into_mut()),
            Entry::Vacant(entry) => {
                let rendered = self
                    .font
                    .render_char(ch)
                    .blended(Color::WHITE)
                    .with_context(|| format!("error rendering {}", ch))?;
                let mut tex = rendered.as_texture(self.texture_creator)?;
                tex.set_blend_mode(BlendMode::Blend);
                Ok(entry.insert(tex))
            }
        }
    }
}
