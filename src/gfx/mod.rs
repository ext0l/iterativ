mod layer;
mod log_layer;
mod map_layer;
mod sidebar_layer;
mod targeting_layer;
mod text_grid;
mod visibles_layer;

use std::{cell::RefCell, rc::Rc};

use anyhow::{anyhow, Context, Result};

use layer::MapGeometry;
use log::info;
use log_layer::{LogCache, LogLayer};
use map_layer::MapLayer;
use sdl2::{
    render::{TextureCreator, WindowCanvas},
    ttf::Font,
    video::WindowContext,
};
use sidebar_layer::SidebarLayer;
use targeting_layer::TargetingLayer;
use visibles_layer::VisiblesLayer;

use crate::geometry::*;
use crate::map::Map;
use crate::{engine::Engine, event_log::EventLog, tiles::Tiles};

pub struct GfxOptions {
    pub show_out_of_bounds: bool,
}

impl GfxOptions {
    pub fn new() -> Self {
        Self {
            show_out_of_bounds: false,
        }
    }
}

pub struct Gfx<'a> {
    texture_creator: &'a TextureCreator<WindowContext>,
    map_geometry: MapGeometry,
    map_layer: MapLayer<'a>,
    visibles_layer: VisiblesLayer<'a>,
    targeting_layer: TargetingLayer<'a>,
    map_region: ScreenBox,
    log_layer: LogLayer<'a>,
    log_region: ScreenBox,
    sidebar_layer: SidebarLayer<'a>,
    sidebar_region: ScreenBox,
    /// Where in worldspace is the center of the map in cameraspace located?
    options: GfxOptions,
}

impl<'a> Gfx<'a> {
    pub fn new(
        texture_creator: &'a TextureCreator<WindowContext>,
        tiles: Rc<RefCell<Tiles<'a>>>,
        map_region: ScreenBox,
        log_region: ScreenBox,
        sidebar_region: ScreenBox,
        font: &'a Font<'a, 'static>,
    ) -> Self {
        let tiles_size = tiles.borrow().size();
        let camera_size = CameraSize::new(
            map_region.width() / tiles_size.width,
            map_region.height() / tiles_size.height,
        );
        let map_geometry = MapGeometry {
            tile_size: tiles_size,
            camera_size,
            camera_center: WorldPoint::zero(),
        };
        let sidebar_layer =
            SidebarLayer::new(font, texture_creator, sidebar_region.size()).unwrap();
        let targeting_layer =
            TargetingLayer::new(texture_creator, &map_geometry, tiles.clone()).unwrap();

        Self {
            texture_creator,
            log_region,
            map_region,
            map_geometry,
            map_layer: MapLayer::new(Rc::clone(&tiles)),
            visibles_layer: VisiblesLayer::new(Rc::clone(&tiles)),
            targeting_layer,
            log_layer: LogLayer::new(font, log_region.size()),
            sidebar_layer,
            sidebar_region,
            options: GfxOptions::new(),
        }
    }

    /// Note that just getting a mutable reference to the options will
    /// cause all texture caches to be dropped.
    pub fn options_mut(&mut self) -> &mut GfxOptions {
        info!("Modifying options; flushing the map cache");
        self.map_layer.clear_cache();
        &mut self.options
    }

    pub fn set_camera_center(&mut self, pos: WorldPoint) {
        self.map_geometry.camera_center = pos;
    }

    pub fn draw(&mut self, canvas: &mut WindowCanvas, state: &Engine) -> Result<()> {
        if state.dirty_layers().map {
            self.map_layer.clear_cache();
        }

        let map = &*state.resources.get::<Map>()?;
        let map_region = self.map_region;
        let map_tex = self.map_layer.render(
            canvas,
            self.texture_creator,
            &self.options,
            &self.map_geometry,
            map,
        )?;
        canvas
            .copy(map_tex, None, sdl_rect(map_region.min, map_region.size()))
            .map_err(|e| anyhow!("Failed to copy map texture: {}", e))?;
        let targeting_tex = self.targeting_layer.render(
            canvas,
            &self.map_geometry,
            &state.world,
            &state.resources,
        )?;
        canvas
            .copy(
                &targeting_tex,
                None,
                sdl_rect(map_region.min, map_region.size()),
            )
            .map_err(|e| anyhow!("Failed to copy map texture: {}", e))?;
        let visibles_tex = self.visibles_layer.render(
            canvas,
            self.texture_creator,
            &self.map_geometry,
            &state.world,
            &state.resources,
        )?;
        canvas
            .copy(
                &visibles_tex,
                None,
                sdl_rect(map_region.min, map_region.size()),
            )
            .map_err(|e| anyhow!("Failed to copy map texture: {}", e))?;

        let sidebar_tex = self
            .sidebar_layer
            .render(canvas, &state.world, &state.resources)?;
        canvas
            .copy(
                &sidebar_tex,
                None,
                sdl_rect(self.sidebar_region.min, self.sidebar_region.size()),
            )
            .map_err(|e| anyhow!("Failed to copy sidebar texture: {}", e))?;

        let event_log = state.resources.get::<EventLog>()?;
        if let Some(LogCache { texture, size, .. }) = self
            .log_layer
            .render(self.texture_creator, &event_log)
            .context("failed to render event log")?
        {
            let size = size.cast::<u32>();
            let log_region = sdl2::rect::Rect::new(
                self.log_region.min.x,
                self.log_region.min.y,
                size.width,
                size.height,
            );
            canvas
                .copy(&texture, None, Some(log_region))
                .map_err(|e| anyhow!("Failed to copy texture: {}", e))?;
        }

        canvas.present();
        Ok(())
    }
}

pub fn sdl_rect(min: ScreenPoint, size: ScreenSize) -> sdl2::rect::Rect {
    let size = size.to_u32();
    sdl2::rect::Rect::new(min.x, min.y, size.width, size.height)
}
