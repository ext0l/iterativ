use std::{cell::RefCell, rc::Rc};

use anyhow::Result;

use hecs::World;
use resources::Resources;
use sdl2::{
    pixels::Color, render::BlendMode, render::Texture, render::TextureCreator,
    render::WindowCanvas, video::WindowContext,
};

use crate::{components::*, geometry::*, map::Map};
use crate::{systems::Particle, tiles::Tiles};

use super::{
    layer::RenderTargetGuard,
    layer::{create_texture, MapGeometry},
    sdl_rect,
};

pub struct VisiblesLayer<'a> {
    tiles: Rc<RefCell<Tiles<'a>>>,
}

impl<'a> VisiblesLayer<'a> {
    pub fn new(tiles: Rc<RefCell<Tiles<'a>>>) -> Self {
        Self { tiles }
    }

    pub fn render(
        &mut self,
        canvas: &mut WindowCanvas,
        texture_creator: &'a TextureCreator<WindowContext>,
        map_geometry: &MapGeometry,
        world: &World,
        resources: &Resources,
    ) -> Result<Texture> {
        let texture = Rc::new(create_texture(texture_creator, map_geometry.screen_size())?);
        let mut guard = RenderTargetGuard::new(canvas, texture.clone())?;
        let canvas = guard.canvas();
        let map = resources.get::<Map>()?;
        let to_screen = map_geometry.to_screen();

        canvas.set_draw_color(Color::RGBA(0, 0, 0, 0));
        canvas.clear();
        // First, we draw non-particles, then we draw particles on top of them.
        for (_, (pos, vis)) in world
            .query::<(&Position, &Visible)>()
            .without::<Particle>()
            .iter()
        {
            if !map.visible[map.idx(pos.0)] {
                continue;
            }
            self.draw(canvas, to_screen.transform_point(pos.0), vis);
        }
        for (_, (pos, vis)) in world
            .query::<(&Position, &Visible)>()
            .with::<Particle>()
            .iter()
        {
            self.draw(canvas, to_screen.transform_point(pos.0), vis);
        }
        drop(guard);
        Ok(Rc::try_unwrap(texture)
            .map_err(|_| "could not unwrap")
            .unwrap())
    }

    fn draw(&self, canvas: &mut WindowCanvas, position: ScreenPoint, vis: &Visible) {
        let mut tiles = self.tiles.borrow_mut();
        let dest = sdl_rect(position, tiles.size());
        let texture = tiles.texture_mut(vis.tile_id);
        texture.set_alpha_mod((255.0 * vis.alpha).round() as u8);
        texture.set_color_mod(vis.color.r, vis.color.g, vis.color.b);
        canvas.set_blend_mode(BlendMode::None);
        canvas
            .copy(texture, None, Some(dest))
            .expect("failed to copy tile");
    }
}
