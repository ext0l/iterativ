use std::{cell::RefCell, rc::Rc};

use anyhow::{anyhow, Context, Result};
use hecs::World;
use resources::Resources;
use sdl2::{
    pixels::Color,
    render::WindowCanvas,
    render::{Texture, TextureCreator},
    video::WindowContext,
};

use crate::{
    components::Position,
    engine::{LoopState, PlayerId},
    geometry::WorldVector,
    map::Map,
    tiles::{TileId, Tiles},
};

use super::layer::{create_texture, MapGeometry, RenderTargetGuard};

const TARGETING_COLOR: Color = Color::RGBA(0, 0, 0, 200);

pub struct TargetingLayer<'a> {
    texture: Rc<Texture<'a>>,
    tiles: Rc<RefCell<Tiles<'a>>>,
}

impl<'a> TargetingLayer<'a> {
    pub fn new(
        texture_creator: &'a TextureCreator<WindowContext>,
        map_geometry: &MapGeometry,
        tiles: Rc<RefCell<Tiles<'a>>>,
    ) -> Result<Self> {
        let texture = Rc::new(
            create_texture(texture_creator, map_geometry.screen_size())
                .context("couldn't create backing texture for targeting layer")?,
        );
        Ok(Self { texture, tiles })
    }

    pub fn render(
        &mut self,
        canvas: &mut WindowCanvas,
        map_geometry: &MapGeometry,
        world: &World,
        resources: &Resources,
    ) -> Result<&Texture<'a>> {
        let mut guard = RenderTargetGuard::new(canvas, self.texture.clone())?;
        let canvas = guard.canvas();
        canvas.set_draw_color(Color::RGBA(0, 0, 0, 0));
        canvas.clear();

        if let LoopState::Targeting(ref targeting) = *resources.get()? {
            canvas.set_draw_color(TARGETING_COLOR);
            canvas.clear();
            let map = resources.get::<Map>().context("couldn't get map")?;
            let range = targeting.range;
            let player = resources
                .get::<PlayerId>()
                .context("couldn't get player")?
                .0;
            let player_pos = world
                .get::<Position>(player)
                .context("couldn't get player position")?
                .0;
            canvas.set_draw_color(Color::RGBA(0, 0, 0, 0));
            for dx in -range..=range {
                for dy in -range..=range {
                    let pos = player_pos + WorldVector::new(dx, dy);
                    if !(map.is_valid(pos) && map.visible[map.idx(pos)]) {
                        continue;
                    }
                    canvas
                        .fill_rect(map_geometry.tile_rect(pos))
                        .map_err(|e| anyhow!("couldn't fill rect at {:?}: {}", pos, e))?;
                }
            }
            if let Some(target) = targeting.target() {
                let target_pos = world
                    .get::<Position>(target)
                    .context("couldn't get target position")?
                    .0;
                let tiles = self.tiles.borrow_mut();
                let reticule = tiles.texture(TileId::Reticule);
                canvas
                    .copy(reticule, None, Some(map_geometry.tile_rect(target_pos)))
                    .map_err(|e| anyhow!("couldn't draw reticule at {:?}: {}", target_pos, e))?;
            }
        }

        Ok(&self.texture)
    }
}
