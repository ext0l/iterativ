use std::{cell::RefCell, rc::Rc};

use anyhow::{anyhow, Result};

use log::debug;
use sdl2::{
    pixels::Color,
    render::BlendMode,
    render::TextureCreator,
    render::{Texture, WindowCanvas},
    video::WindowContext,
};

use crate::{geometry::*, map::Map, map::TileType, tiles::TileId, tiles::Tiles};

use super::{
    layer::create_texture, layer::MapGeometry, layer::RenderTargetGuard, sdl_rect, GfxOptions,
};

pub struct MapLayer<'a> {
    tiles: Rc<RefCell<Tiles<'a>>>,
    cache: Option<Rc<Texture<'a>>>,
}

impl<'a> MapLayer<'a> {
    pub fn new(tiles: Rc<RefCell<Tiles<'a>>>) -> Self {
        Self { tiles, cache: None }
    }

    pub fn clear_cache(&mut self) {
        debug!("Clearing map cache");
        self.cache = None;
    }

    /// Which tile should we draw here, if any?
    fn which_tile(&self, options: &GfxOptions, map: &Map, point: WorldPoint) -> Option<TileId> {
        if !map.is_valid(point) {
            if options.show_out_of_bounds {
                return Some(TileId::OutOfBounds);
            } else {
                return None;
            }
        }
        let idx = map.idx(point);
        if !(map.visible[idx] || map.revealed[idx]) {
            return None;
        }
        match map.tiles[idx] {
            TileType::Floor => Some(TileId::Floor),
            TileType::Wall => Some(TileId::Wall),
        }
    }

    pub fn render(
        &mut self,
        canvas: &mut WindowCanvas,
        texture_creator: &'a TextureCreator<WindowContext>,
        options: &GfxOptions,
        map_geometry: &MapGeometry,
        map: &Map,
    ) -> Result<&Texture> {
        if let Some(ref tex) = self.cache {
            return Ok(tex);
        }
        let texture = Rc::new(create_texture(texture_creator, map_geometry.screen_size())?);
        let mut guard = RenderTargetGuard::new(canvas, texture.clone())?;
        let canvas = guard.canvas();
        let to_camera = map_geometry.to_screen();
        let tiles = self.tiles.borrow();
        canvas.set_draw_color(Color::BLACK);
        canvas.clear();
        for point in map_geometry.displayed_points_iter() {
            if let Some(tile) = self.which_tile(options, map, point) {
                let dest = sdl_rect(to_camera.transform_point(point), tiles.size());
                canvas
                    .copy(tiles.texture(tile), None, Some(dest))
                    .map_err(|e| anyhow!("couldn't copy tile at {:?}: {}", point, e))?;
            }
        }

        // Now draw the visibility mask.
        canvas.set_blend_mode(BlendMode::Blend);
        canvas.set_draw_color(Color::RGBA(50, 50, 50, 240));
        for point in map_geometry
            .displayed_points_iter()
            .filter(|p| map.is_valid(*p))
        {
            let idx = map.idx(point);
            if map.revealed[idx] && !map.visible[idx] {
                let dest = sdl_rect(to_camera.transform_point(point), tiles.size());
                canvas
                    .fill_rect(dest)
                    .map_err(|e| anyhow!("error in visibility shading at {:?}: {}", point, e))?;
            }
        }
        self.cache = Some(texture);
        Ok(self.cache.as_ref().unwrap())
    }
}
