use anyhow::Result;
use hecs::World;
use resources::Resources;
use sdl2::{
    pixels::Color, render::Texture, render::TextureCreator, render::WindowCanvas, ttf::Font,
    video::WindowContext,
};

use crate::{components::CombatStats, engine::PlayerId, geometry::GridPoint, geometry::ScreenSize};

use super::text_grid::TextGrid;

pub struct SidebarLayer<'a> {
    text_grid: TextGrid<'a>,
}

impl<'a> SidebarLayer<'a> {
    pub fn new(
        font: &'a Font<'a, 'static>,
        texture_creator: &'a TextureCreator<WindowContext>,
        size: ScreenSize,
    ) -> Result<Self> {
        let text_grid = TextGrid::new(size, texture_creator, font)?;
        Ok(Self { text_grid })
    }

    pub fn render(
        &mut self,
        canvas: &mut WindowCanvas,
        world: &World,
        resources: &Resources,
    ) -> Result<&Texture<'a>> {
        let player_id = *resources.get::<PlayerId>()?;
        let player_stats = world.get::<CombatStats>(player_id.0)?;
        self.text_grid.clear();
        let end = self
            .text_grid
            .draw_text(GridPoint::zero(), "HP: ", Color::WHITE);
        self.text_grid.draw_text(
            end,
            &format!("{}/{}", player_stats.hp, player_stats.max_hp),
            ratio_color(player_stats.hp, player_stats.max_hp),
        );
        self.text_grid.render(canvas)
    }
}

fn ratio_color(value: i32, max: i32) -> Color {
    let ratio = (value as f32) / (max as f32);
    if ratio > 0.6 {
        Color::GREEN
    } else if ratio > 0.3 {
        Color::YELLOW
    } else {
        Color::RED
    }
}
