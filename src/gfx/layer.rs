use std::rc::Rc;

use anyhow::{bail, Result};
use euclid::Transform2D;
use itertools::Itertools;
use sdl2::{
    pixels::PixelFormatEnum,
    render::BlendMode,
    render::Canvas,
    render::RenderTarget,
    render::TargetRenderError,
    render::{Texture, TextureCreator},
    video::WindowContext,
};

use crate::geometry::*;

use super::sdl_rect;

pub fn create_texture<'tex>(
    texture_creator: &'tex TextureCreator<WindowContext>,
    size: ScreenSize,
) -> Result<Texture<'tex>> {
    let size = size.cast::<u32>();
    let mut tex = texture_creator.create_texture_target(
        PixelFormatEnum::ARGB8888,
        size.width,
        size.height,
    )?;
    tex.set_blend_mode(BlendMode::Blend);
    Ok(tex)
}

/// A guard-style wrapper around SetRenderTarget.
///
/// This takes an Rc of the texture in order to ensure that the
/// texture is valid, even if this leaks.
///
/// Safety: This has a reference to the canvas, so it can't possibly
/// outlive it. It also has a reference to the current render target,
/// so it can't outlive that either.
pub struct RenderTargetGuard<'a, T: RenderTarget + 'a> {
    canvas: &'a mut Canvas<T>,
    target: Option<Rc<Texture<'a>>>,
    original_target: *mut sdl2::sys::SDL_Texture,
}

impl<'a, T: RenderTarget + 'a> RenderTargetGuard<'a, T> {
    /// Create a new target guard, but don't set the render target.
    pub fn new_unset(canvas: &'a mut Canvas<T>) -> Result<Self> {
        if !canvas.render_target_supported() {
            bail!(TargetRenderError::NotSupported);
        }
        let original_target = unsafe { sdl2::sys::SDL_GetRenderTarget(canvas.raw()) };
        Ok(Self {
            canvas,
            target: None,
            original_target,
        })
    }

    /// Creates a render guard and sets its target.
    pub fn new(canvas: &'a mut Canvas<T>, target: Rc<Texture<'a>>) -> Result<Self> {
        let mut this = Self::new_unset(canvas)?;
        this.set_target(target)?;
        Ok(this)
    }

    /// Sets the render guard's target.
    pub fn set_target(&mut self, target: Rc<Texture<'a>>) -> Result<()> {
        let raw = target.raw();
        self.target = Some(target);
        let result = unsafe { sdl2::sys::SDL_SetRenderTarget(self.canvas.raw(), raw) };
        if result == 0 {
            Ok(())
        } else {
            bail!("Could not SetRenderTarget: {}", sdl2::get_error())
        }
    }

    pub fn canvas(&mut self) -> &mut Canvas<T> {
        self.canvas
    }
}

impl<'a, T: RenderTarget + 'a> Drop for RenderTargetGuard<'a, T> {
    fn drop(&mut self) {
        let result =
            unsafe { sdl2::sys::SDL_SetRenderTarget(self.canvas.raw(), self.original_target) };
        if result != 0 {
            panic!("Could not set render target back: {}", sdl2::get_error())
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub struct MapGeometry {
    /// Size of an individual tile.
    pub tile_size: ScreenSize,
    /// Size of the region that the camera draws.
    pub camera_size: CameraSize,
    /// Worldspace location of the center of the camera.
    pub camera_center: WorldPoint,
}

impl MapGeometry {
    /// Transforms cameraspace to worldspace.
    pub fn to_world(&self) -> Transform2D<i32, CameraSpace, WorldSpace> {
        // find the center of the region in cameraspace...
        let region_center = self.camera_size.to_vector() / 2;
        Transform2D::identity()
            .then_translate(-region_center)
            .with_destination::<WorldSpace>()
            .then_translate(self.camera_center.to_vector())
    }

    /// Transforms worldspace to screenspace.
    pub fn to_screen(&self) -> Transform2D<i32, WorldSpace, ScreenSpace> {
        // transform to worldspace, then scale up by the size of the tiles
        self.to_world()
            .inverse()
            .unwrap()
            .then_scale(self.tile_size.width, self.tile_size.height)
            .with_destination::<ScreenSpace>()
    }

    /// An iterator over all map points that are being displayed
    /// onscreen, including points that don't exist.
    pub fn displayed_points_iter(&self) -> impl Iterator<Item = WorldPoint> {
        let to_world = self.to_world();
        (0..self.camera_size.height)
            .cartesian_product(0..self.camera_size.width)
            .map(move |(y, x)| to_world.transform_point(CameraPoint::new(x, y)))
    }

    /// Size of the displayed region in pixels.
    pub fn screen_size(&self) -> ScreenSize {
        ScreenSize::new(
            self.camera_size.width * self.tile_size.width,
            self.camera_size.height * self.tile_size.height,
        )
    }

    pub fn tile_rect(&self, point: WorldPoint) -> sdl2::rect::Rect {
        let point = self.to_screen().transform_point(point);
        sdl_rect(point, self.tile_size)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn to_world() {
        let geo = MapGeometry {
            tile_size: ScreenSize::new(10, 15),
            camera_size: CameraSize::new(80, 40),
            camera_center: WorldPoint::new(30, 40),
        };
        assert_eq!(
            WorldPoint::new(-10, 20),
            geo.to_world().transform_point(CameraPoint::zero())
        );
        assert_eq!(
            WorldPoint::new(30, 40),
            geo.to_world().transform_point(CameraPoint::new(40, 20))
        );
    }

    #[test]
    fn to_screen() {
        let geo = MapGeometry {
            tile_size: ScreenSize::new(10, 15),
            camera_size: CameraSize::new(80, 40),
            camera_center: WorldPoint::new(30, 40),
        };
        let world_point = WorldPoint::new(35, 50);
        assert_eq!(
            ScreenPoint::new(450, 450),
            geo.to_screen().transform_point(world_point)
        );
        assert_eq!(
            ScreenPoint::zero(),
            geo.to_screen().transform_point(WorldPoint::new(-10, 20))
        );
    }
}
