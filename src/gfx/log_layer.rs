use anyhow::{Context, Result};
use sdl2::{
    pixels::Color, render::Texture, render::TextureCreator, ttf::Font, video::WindowContext,
};

use crate::{event_log::EventLog, geometry::ScreenSize};

pub struct LogLayer<'a> {
    cache: Option<LogCache<'a>>,
    font: &'a Font<'a, 'static>,
    screen_size: ScreenSize,
}

pub struct LogCache<'a> {
    pub text: String,
    pub texture: Texture<'a>,
    pub size: ScreenSize,
}

impl<'a> LogLayer<'a> {
    pub fn new(font: &'a Font<'a, 'static>, screen_size: ScreenSize) -> Self {
        Self {
            cache: None,
            font,
            screen_size,
        }
    }

    /// Whether the given log string's rendering is cached.
    fn is_cached(&self, text: &str) -> bool {
        match &self.cache {
            Some(cache) => cache.text == text,
            None => false,
        }
    }

    /// Render the event log to a surface.
    ///
    /// If the event log is empty, `Ok(None)` is returned, since there's no surface to be drawn.
    pub fn render(
        &mut self,
        texture_creator: &'a TextureCreator<WindowContext>,
        log: &EventLog,
    ) -> Result<Option<&LogCache<'a>>> {
        let max_lines = (self.screen_size.height / self.font.height()) as usize;
        let mut lines: Vec<_> = log.events().take(max_lines).collect();
        lines.reverse();
        let joined = lines.join("\n");
        if joined.is_empty() {
            return Ok(None);
        }

        if !self.is_cached(&joined) {
            let surface = self
                .font
                .render(&joined)
                .blended_wrapped(Color::WHITE, self.screen_size.width as u32)
                .context("Failed to render event log")?;
            let texture = texture_creator
                .create_texture_from_surface(surface)
                .context("failed converting log surface to texture")?;
            let query = texture.query();
            let size = ScreenSize::new(
                self.screen_size.width.min(query.width as i32),
                self.screen_size.height.min(query.height as i32),
            );
            self.cache = Some(LogCache {
                text: joined,
                texture,
                size,
            });
        }
        return Ok(Some(&self.cache.as_ref().unwrap()));
    }
}
