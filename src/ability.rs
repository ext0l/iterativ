use crate::engine::TargetingMode;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Ability {
    /// Moves the player adjacent to the target.
    GapCloser,
}

impl Ability {
    pub fn range(self) -> i32 {
        match self {
            Self::GapCloser => 5,
        }
    }

    pub fn targeting_mode(self) -> TargetingMode {
        match self {
            Self::GapCloser => TargetingMode::Enemy,
        }
    }
}
