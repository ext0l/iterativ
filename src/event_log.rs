use crate::components::Name;

use hecs::{Entity, World};
use log::*;
use std::collections::VecDeque;

/// Something that happened that should be displayed in the game's log.
#[derive(Clone, Debug)]
pub enum Event {
    Damage {
        from: Entity,
        to: Entity,
        amount: i32,
    },
    Death {
        who: Entity,
    },
    #[allow(dead_code)]
    Other(String),
}

impl Event {
    pub fn format(&self, world: &World) -> String {
        let lookup = |who: &Entity| {
            world
                .get_mut::<Name>(*who)
                .ok()
                .map_or_else(|| "an unnamed bug".to_string(), |name| name.name.clone())
        };
        match self {
            Event::Damage { from, to, amount } => format!(
                "{} hits {} for {} damage.",
                lookup(from),
                lookup(to),
                amount
            ),
            Event::Death { who } => format!("{} dies.", lookup(who)),
            Event::Other(message) => message.clone(),
        }
    }
}

#[derive(Debug)]
pub struct EventLog {
    // The list of events. The head of the queue is the oldest event, and the tail is the newest
    // one.
    events: VecDeque<String>,
    capacity: usize,
}

impl EventLog {
    pub fn new() -> Self {
        EventLog {
            events: VecDeque::new(),
            capacity: 1000,
        }
    }

    pub fn log(&mut self, event: String) {
        info!("Logging event: {:?}", event);
        self.events.push_back(event);
    }

    /// Returns the events, from *newest to oldest*. This is the order that they should be rendered
    /// in.
    pub fn events(&self) -> impl Iterator<Item = &str> {
        self.events.iter().map(|s| s.as_str()).rev()
    }
}
