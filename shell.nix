with import <nixpkgs> {};

let
  moz_overlay = import (builtins.fetchTarball
    "https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz");
  nixpkgs = import <nixpkgs> { overlays = [ moz_overlay ]; };
  rustStable = nixpkgs.latest.rustChannels.stable.rust.override {
    extensions =
      [ "rustfmt-preview" "clippy-preview" ];
  };
in with nixpkgs;
stdenv.mkDerivation rec {
  name = "moz_overlay_shell";
  buildInputs = [
    libGL
    libudev
    xorg.libX11
    xorg.libXcursor
    xorg.libXrandr
    xorg.libXi
    rust-analyzer
    rustStable
    pkg-config
    openssl
    cargo-web
    SDL2
    SDL2_image
    SDL2_ttf
  ];
  RUST_BACKTRACE = 1;
  LD_LIBRARY_PATH = "${lib.makeLibraryPath buildInputs}";
}
